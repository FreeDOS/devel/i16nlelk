# IA-16 Newlib support libraries for ELKS

Libraries to build ELKS OS programs with GCC IA-16 and Newlib

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## I16NLELK.LSM

<table>
<tr><td>title</td><td>IA-16 Newlib support libraries for ELKS</td></tr>
<tr><td>version</td><td>2.4.0-20191005a</td></tr>
<tr><td>entered&nbsp;date</td><td>2019-10-06</td></tr>
<tr><td>description</td><td>Libraries to build ELKS OS programs with GCC IA-16 and Newlib</td></tr>
<tr><td>keywords</td><td>gcc, compiler, elks, newlib, libelks, djgpp, ia16, elf</td></tr>
<tr><td>maintained&nbsp;by</td><td>TK Chia</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/tkchia/build-ia16/releases/</td></tr>
<tr><td>original&nbsp;site</td><td>https://github.com/tkchia/newlib-ia16/</td></tr>
<tr><td>platforms</td><td>DOS, Linux</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, version 3 or later</td></tr>
</table>
